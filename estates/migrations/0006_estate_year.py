# Generated by Django 4.0 on 2021-12-22 21:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('estates', '0005_alter_estate_estate_type'),
    ]

    operations = [
        migrations.AddField(
            model_name='estate',
            name='year',
            field=models.CharField(blank=True, max_length=20, null=True),
        ),
    ]
